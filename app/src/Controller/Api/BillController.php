<?php

namespace App\Controller\Api;

use App\Service\BillServiceInterface;
use Symfony\Component\HttpFoundation\{JsonResponse, Response};
use App\Helper\DTO\Response\BillResponseDTO;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;

#[OA\Tag(name: 'Bills.')]
class BillController extends AbstractController
{
    public function __construct(
        private readonly BillServiceInterface $billService,
    )
    {
    }

    #[OA\Get(
        description: 'Получение всех счетов, имеющихся в БД.',
        summary: 'Получение всех счетов.',
        responses: [
            new OA\Response(
                response: Response::HTTP_OK,
                description: 'Получение счетов.',
                content: new OA\JsonContent(ref: new Model(type: BillResponseDTO::class, groups: ['getList']))
            ),
        ]
    )]
    #[Route('/bills', name: 'getListBills', methods: ['GET'])]
    public function getList(): JsonResponse
    {
        return $this->json($this->billService->getList());
    }
}
