<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\{JsonResponse, Request, Response};
use App\Entity\User;
use App\Helper\Attributes\NoValidDataResponse;
use App\Service\PartyServiceInterface;
use App\Helper\DTO\CRUD\PartyDTO;
use App\Helper\DTO\Response\PartyResponseDTO;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;

#[OA\Tag(name: 'Parties.')]
#[Route(path: '/parties')]
final class PartyController extends AbstractController
{
    public function __construct(
        private readonly PartyServiceInterface $partyService,
    )
    {
    }

    #[OA\Get(
        description: 'Получение списка тусовок - никаких параметров, просто запрос.',
        summary: 'Получение списка тусовок.',
        responses: [
            new OA\Response(
                response: Response::HTTP_OK,
                description: 'Список тусовок',
                content: new OA\JsonContent(
                    properties: [
                        new OA\Property(
                            property: 'data',
                            type: 'array',
                            items: new OA\Items(
                                ref: new Model(type: PartyResponseDTO::class, groups: ['getList'])
                            ),
                        ),
                    ],
                    type: 'object',
                )
            )
        ]
    )]
    #[Route(path: '/', name: 'getListParties', methods: [Request::METHOD_GET])]
    public function getListParties(): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        return $this->json(
            data: ['data' => $this->partyService->getList()],
            context: ['groups' => ['getList']],
        );
    }

    #[OA\Post(
        description: 'Создание тусовки, все необходимые поля - в теле запроса.',
        summary: 'Создание тусовки.',
        requestBody: new OA\RequestBody(
            required: true,
            content: new OA\JsonContent(ref: new Model(type: PartyDTO::class, groups: ['create'])),
        ),
        responses: [
            new OA\Response(
                response: Response::HTTP_CREATED,
                description: 'Тусовка создана.',
                content: new OA\JsonContent(ref: new Model(type: PartyResponseDTO::class, groups: ['create']))
            ),
        ]
    )]
    #[NoValidDataResponse]
    #[Route(path: '/', name: 'createParty', methods: [Request::METHOD_POST])]
    public function createParty(
        #[MapRequestPayload(validationGroups: ['create'])] PartyDTO $partyDTO,
    ): JsonResponse
    {
        return $this->json(
            data: $this->partyService->create($partyDTO),
            context: ['groups' => ['create']],
        );
    }
}
