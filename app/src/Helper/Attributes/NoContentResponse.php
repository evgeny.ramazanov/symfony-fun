<?php

namespace App\Helper\Attributes;

use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

#[\Attribute] final class NoContentResponse extends OA\Response
{
    public function __construct()
    {
        parent::__construct(
            response: Response::HTTP_NO_CONTENT,
            description: 'OK.',
        );
    }
}
