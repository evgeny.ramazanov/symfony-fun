<?php

namespace App\Helper\Attributes;

use Symfony\Component\HttpFoundation\Response;
use OpenApi\Attributes as OA;

#[\Attribute] final class ForbiddenExceptionResponse extends OA\Response
{
    public function __construct()
    {
        parent::__construct(
            response: Response::HTTP_FORBIDDEN,
            description: 'Недостаточно прав для выполнения операции.',
            content: new OA\JsonContent(ref: '#/components/schemas/ApiException'),
        );
    }
}
