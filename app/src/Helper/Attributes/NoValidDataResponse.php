<?php

namespace App\Helper\Attributes;

use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

#[\Attribute] class NoValidDataResponse extends OA\Response
{
    public function __construct()
    {
        parent::__construct(
            response: Response::HTTP_BAD_REQUEST,
            description: 'Некорректный запрос.',
            content: new OA\JsonContent(ref: '#/components/schemas/ApiException'),
        );
    }
}
