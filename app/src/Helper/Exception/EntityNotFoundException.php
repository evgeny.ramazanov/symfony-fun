<?php

namespace App\Helper\Exception;

use Symfony\Component\HttpFoundation\Response;

class EntityNotFoundException extends ApiException
{
    private const MESSAGE = 'Сущность не найдена.';

    private const DETAIL = 'Entity not found.';

    public function __construct($message = self::MESSAGE, $detail = self::DETAIL)
    {
        parent::__construct(
            message: self::MESSAGE,
            detail: self::DETAIL,
            status: Response::HTTP_NOT_FOUND,
        );
    }
}
