<?php

namespace App\Helper\Mapper;

use App\Entity\Party;
use App\Helper\DTO\Response\PartyResponseDTO;
use Exception;

final readonly class PartyMapper implements MapperInterface
{
    public function castEntityToDTO($entity): PartyResponseDTO
    {
        return new PartyResponseDTO($entity);
    }

    /**
     * @throws Exception
     */
    public function castEntityFromDTO($DTO, $entity = null): Party
    {
        /** @var Party $party */
        $party = $entity ?? new Party();

        $party
            ->setName($DTO->getName())
            ->setDateAt(new \DateTimeImmutable($DTO->getDateAt()));

        if ($DTO->getGuests()) {
            if ($guests = $party->getGuests()) {
                foreach ($guests as $guest) {
                    $party->removeGuest($guest);
                }
            }

            foreach ($DTO->getGuests() as $guest) {
                $party->addGuest($guest);
            }
        }

        if ($DTO->getBills()) {
            if ($bills = $party->getBills()) {
                foreach ($bills as $bill) {
                    $party->removeBill($bill);
                }
            }

            foreach ($DTO->getBills() as $bill) {
                $party->addBill($bill);
            }
        }

        return $party;
    }
}
