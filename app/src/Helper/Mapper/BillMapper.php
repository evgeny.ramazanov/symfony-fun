<?php

namespace App\Helper\Mapper;

use App\Entity\Bill;
use App\Helper\DTO\Response\BillResponseDTO;
use Exception;

final readonly class BillMapper implements MapperInterface
{
    public function castEntityToDTO($entity): BillResponseDTO
    {
        return new BillResponseDTO($entity);
    }

    /**
     * @throws Exception
     */
    public function castEntityFromDTO($DTO, $entity = null): Bill
    {
        /** @var Bill $party */
        $bill = $entity ?? new Bill();

        $bill->setCost($DTO->getCost());

        return $bill;
    }
}
