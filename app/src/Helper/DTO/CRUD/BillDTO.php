<?php

namespace App\Helper\DTO\CRUD;

use Symfony\Component\Serializer\Annotation\Groups;

final readonly class BillDTO
{
    #[Groups(['create'])]
    public string $cost;


    public function getCost(): string
    {
        return $this->cost;
    }
}
