<?php

namespace App\Helper\DTO\CRUD;

use Symfony\Component\Serializer\Annotation\Groups;

final class PartyDTO
{
    #[Groups(['create'])]
    public string $name;

    #[Groups(['create'])]
    public string $dateAt;

    public array $guests = [];

    public array $bills = [];


    public function getName(): string
    {
        return $this->name;
    }

    public function getDateAt(): string
    {
        return $this->dateAt;
    }

    public function getGuests(): array
    {
        return $this->guests;
    }

    public function getBills(): array
    {
        return $this->bills;
    }
}
