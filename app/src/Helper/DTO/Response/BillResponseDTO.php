<?php

namespace App\Helper\DTO\Response;

use App\Entity\Bill;
use Symfony\Component\Serializer\Annotation\Groups;

final readonly class BillResponseDTO
{
    #[Groups(['getList'])]
    public int $id;

    #[Groups(['getList'])]
    public float $cost;


    public function __construct(Bill $bill)
    {
        $this->id = $bill->getId();
        $this->cost = $bill->getCost();
    }
}
