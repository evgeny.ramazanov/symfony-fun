<?php

namespace App\Helper\DTO\Response;

use App\Entity\Party;
use Symfony\Component\Serializer\Annotation\Groups;

final readonly class PartyResponseDTO
{
    #[Groups(['getList', 'create'])]
    public string $name;

    #[Groups(['getList', 'create'])]
    public \DateTimeImmutable $dateAt;


    public function __construct(Party $party)
    {
        $this->name = $party->getName();
        $this->dateAt = $party->getDateAt();
    }
}
