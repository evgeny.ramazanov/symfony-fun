<?php

namespace App\Service;

use App\Entity\Party;
use App\Helper\DTO\CRUD\PartyDTO;
use App\Helper\DTO\Response\PartyResponseDTO;

interface PartyServiceInterface extends ServiceInterface
{
    public function create(PartyDTO $partyDTO): PartyResponseDTO;

    public function update(PartyDTO $partyDTO, Party $party): PartyResponseDTO;
}
