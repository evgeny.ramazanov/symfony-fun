<?php

namespace App\Service;

use App\Entity\Party;
use App\Helper\DTO\CRUD\PartyDTO;
use App\Helper\DTO\Response\PartyResponseDTO;
use App\Helper\Exception\EntityNotFoundException;
use App\Helper\Mapper\MapperInterface;
use Doctrine\ORM\EntityManagerInterface;

final readonly class PartyService implements PartyServiceInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private MapperInterface        $mapper,
    )
    {
    }

    public function getList(): array
    {

        $partiesEntities = $this->entityManager->getRepository(Party::class)->findAll();

        $partyDTOs = [];

        foreach ($partiesEntities as $party) {
            $partyDTOs[] = $this->mapper->castEntityToDTO($party);
        }

        return $partyDTOs;
    }

    public function create(PartyDTO $partyDTO): PartyResponseDTO
    {
        $party = $this->mapper->castEntityFromDTO($partyDTO);

        $this->entityManager->persist($party);
        $this->entityManager->flush();

        return $this->mapper->castEntityToDTO($party);
    }

    public function update(PartyDTO $partyDTO, Party $party): PartyResponseDTO
    {
        $party = $this->mapper->castEntityFromDTO($partyDTO, $party);

        $this->entityManager->persist($party);
        $this->entityManager->flush();

        return $this->mapper->castEntityToDTO($party);
    }

    public function delete($entityId): void
    {
        $entity = $this->entityManager->getRepository(Party::class)->find($entityId);

        if (!$entity) {
            throw new EntityNotFoundException();
        }

        $this->entityManager->remove($entity);
    }
}
