<?php

namespace App\Service;

use App\Entity\Bill;
use App\Helper\DTO\CRUD\BillDTO;
use App\Helper\DTO\Response\BillResponseDTO;

interface BillServiceInterface extends ServiceInterface
{
    public function create(BillDTO $DTO): BillResponseDTO;

    public function update(BillDTO $DTO, Bill $bill): BillResponseDTO;
}
