<?php

namespace App\Service;

use App\Entity\Bill;
use App\Helper\DTO\Response\BillResponseDTO;
use App\Helper\Mapper\MapperInterface;
use Doctrine\ORM\EntityManagerInterface;

final readonly class BillService implements BillServiceInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private MapperInterface        $mapper,
    )
    {
    }

    public function getList(): array
    {
        $billEntities = $this->entityManager->getRepository(Bill::class)->findAll();

        $billDTOs = [];

        foreach ($billEntities as $bill) {
            $billDTOs[] = $this->mapper->castEntityToDTO($bill);
        }

        return $billDTOs;
    }

    public function create($DTO): BillResponseDTO
    {
        return $this->mapper->castEntityFromDTO($DTO);
    }

    public function update($DTO, Bill $bill): BillResponseDTO
    {
        return $this->mapper->castEntityFromDTO($DTO, $bill);
    }

    public function delete($entityId): void
    {
        $bill = $this->entityManager->getRepository(Bill::class)->find($entityId);

        $this->entityManager->remove($bill);
    }
}
